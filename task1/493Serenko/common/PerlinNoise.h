#pragma once

#include <cmath>
#include <random>
#include <vector>

struct Vector {
	float x;
	float y;

	Vector(float _x, float _y) : x(_x), y(_y)
	{}
};

class PerlinNoise {
public:

	PerlinNoise(unsigned int num_of_octaves=1, float persistence=0.5f, int seed=0);
	float perlinNoise2D(float fx, float fy);

private:

	float QunticCurveInterpolation(float a, float b, float x);
	Vector GetPseudoRandomGradientVector(int x,int y);

	float Dot(const Vector& a, const Vector& b) {return a.x * b.x + a.y * b.y;}
	float Noise(float fx, float fy);

	std::vector<int> permutationTable;
	unsigned int numOctaves;
	float persistence;
};