#define GLM_ENABLE_EXPERIMENTAL
#include <glm/vec3.hpp>
// #include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <string>
#include <map>
#include <stack>
#include <vector>

#include <iostream>
#include <GL/gl.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

class	LSystem
{
	struct State
	{
		glm::vec3	pos;
		float	stepDistance;
		glm::vec3	angles;
		float	angle;
		float radius;
		float	invert;
	};
public:
	std::map <char, std::string>	rules;
	std::string	initialString;
	float	startAngle;
	float startDistance;
	float startRadius;
	float	distScale;
	float	angleScale;
	float radiusScale;
	std::string	currentString;
	int numIterations;

	LSystem();
	LSystem (
		std::map <char, std::string>	rules,
		std::string	initialString,
		float	startAngle,
		float startDistance,
		float startRadius,
		float	distScale,
		float	angleScale,
		float radiusScale,
		std::string	currentString,
		int numIterations
	);

	std::vector<float> calculateLineCoords();
	void buildSystem ();
	std::string makeOneStep(const std::string& in);
protected:

	glm::vec3 getNewPosition(State& state);
	void updateState(State& state);
	void addNewCoord(State& state, std::vector<float>& coord);
	void initState(
			State& state,
		  glm::vec3	pos,
			float	stepDistance,
			float radius,
			glm::vec3	angles,
			float	angle,
			float	invert);
};
