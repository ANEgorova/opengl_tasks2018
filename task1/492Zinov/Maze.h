#include <vector>
#include <tuple>
#include <map>
#include <queue>
#include <algorithm>
#include <Mesh.hpp>
#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace std;

const float EPS = 0.1;

class Maze {
    private:
        int size;
        vector<vector<bool> > hor_walls;
        vector<vector<bool> > vert_walls;



    public:
        Maze(int size) : size(size), hor_walls(size - 1, vector<bool>(size, true)),
                         vert_walls(size, vector<bool>(size - 1, true)) {
            srand(time(0));
            
        }

        void generate() {
            vector<tuple<int, int, int, int> > order;
            for (int x = 0; x < size; ++x) {
                for (int y = 0; y < size; ++y) {
                    for (int dx = -1; dx <= 1; ++dx) {
                        for (int dy = -1; dy <= 1; ++dy) {
                            if (!((dx == 0) ^ (dy == 0))) {
                                continue;
                            }
                            if (0 <= x + dx && x + dx < size && 0 <= y + dy && y + dy < size) {
                                order.emplace_back(x, y, dx, dy);
                            }
                        }
                    }
                }
            }
            random_shuffle(order.begin(), order.end());
            map<tuple<int, int, int, int>, int> priority;
            for (int i = 0; i < order.size(); ++i) {
                priority[order[i]] = i;
            }
            priority_queue<tuple<int, tuple<int, int, int, int> > > queue;
            vector<vector<bool> > visited(size, vector<bool>(size, false));
            queue.emplace(0, make_tuple(0, 0, 0, 0));
            while (!queue.empty()) {
                auto move = get<1>(queue.top());
                queue.pop();
                int x = get<0>(move) + get<2>(move);
                int y = get<1>(move) + get<3>(move);
                if (visited[x][y]) { 
                    continue;
                }
                for (int dx = -1; dx <= 1; ++dx) {
                    for (int dy = -1; dy <= 1; ++dy) {
                        if (!((dx == 0) ^ (dy == 0))) {
                            continue;
                        }
                        auto next_move = make_tuple(x, y, dx, dy);
                        if (!priority.count(next_move)) {
                            continue;
                        }
                        queue.emplace(priority[next_move], next_move);
                    }
                }
                if (get<2>(move) != 0 || get<3>(move) != 0) {
                    if (get<3>(move) == 0) {
                        hor_walls[get<0>(move)  - (get<2>(move) < 0)][get<1>(move)] = false;
                    } else {
                        vert_walls[get<0>(move)][get<1>(move)  - (get<3>(move) < 0)] = false;
                    }
                }
                visited[x][y] = true;
            }
        }

        vector<MeshPtr> get_meshes() {
            vector<MeshPtr> meshes;
            meshes.push_back(makeWall(0, 0, false, 10, 5));
            meshes.push_back(makeWall(10, 0, false, 10, 5));
            meshes.push_back(makeWall(0, 0, true, 10, 5));
            meshes.push_back(makeWall(0, 10, true, 10, 5));
            for (int x = 0; x < size; ++x) {
                for (int y = 0; y < size; ++y) {
                    if (x < size - 1 && hor_walls[x][y]) {
                        meshes.push_back(makeWall(x + 1, y, false, 1, 5));
                    }
                    if (y < size - 1 && vert_walls[x][y]) {
                        meshes.push_back(makeWall(x, y + 1, true, 1, 5));
                    }
                }
            }
            return meshes;
        }

        void debug() {
            for (int x = 0; x < size; ++x) {
                for (int y = 0; y < size - 1; ++y) {
                    cout << ' ' << (vert_walls[x][y] ? "|" : " ");
                }
                cout << " " << endl;
                if (x < size - 1) {
                    for (int y = 0; y < size - 1; ++y) {
                        cout << (hor_walls[x][y] ? "_" : " ") << ".";
                    }
                    cout << (hor_walls[x][size - 1] ? "_" : " ") << endl;
                }
            }
        }

        void clamp_pos(glm::vec3 &pos) {
            float xf = pos[0];
            float yf = pos[1] + 1;
            int x = (int)xf;
            int y = (int)yf;
            if (xf - x < EPS && (x == 0 || hor_walls[x - 1][y])) {
                xf = x + 1.1 * EPS;
            }
            if (xf - x > 1 - EPS && (x == size - 1 || hor_walls[x][y])) {
                xf = x + 1 - 1.1 * EPS;
            }
            if (yf - y < EPS && (y == 0 || vert_walls[x][y - 1])) {
                yf = y + 1.1 * EPS;
            }
            if (yf - y > 1 - EPS && (y == size - 1 || vert_walls[x][y])) {
                yf = y + 1 - 1.1 * EPS;
            }
            pos[0] = xf;
            pos[1] = yf - 1;
        }
};
