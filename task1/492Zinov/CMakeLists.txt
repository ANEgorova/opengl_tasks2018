include_directories("common/")

set(SRC_FILES
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    common/DebugOutput.cpp
    Maze.h
    Main.cpp
)


MAKE_TASK(492Zinov 1 "${SRC_FILES}")

