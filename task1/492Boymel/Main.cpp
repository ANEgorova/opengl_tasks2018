//
// Created by Alexander Boymel on 08.03.18.
//

#include "Maze.h"


int main()
{
    MazeApplication app("492BoymelData/maze.txt", 1., false);
    app.start();

    return 0;
}