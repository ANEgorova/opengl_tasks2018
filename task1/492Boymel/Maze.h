//
// Created by Alexander Boymel on 08.03.18.
//

#ifndef STUDENTTASKS2017_MAZE_H
#define STUDENTTASKS2017_MAZE_H

#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <sstream>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Camera.hpp"


// walls: up, bottom, -row, +row, -column, +column

struct LightInfo
{
    glm::vec3 position; //Будем здесь хранить координаты в мировой системе координат, а при копировании в юниформ-переменную конвертировать в систему виртуальной камеры
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
};

struct MaterialInfo
{
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float shininess = 128.0f;
};

constexpr unsigned int LightNum = 1;

class MazeApplication: public Application, public std::enable_shared_from_this<MazeApplication> {
public:
    MazeApplication(std::string _mazeFilename, float _cellSize=1., bool _faceCamera=false);

    void makeScene() override;
    void draw() override;
    void handleKey(int key, int scancode, int action, int mods) override;

    bool possibleMove(glm::vec3 move);

    float getCellSize();

private:
    std::vector<std::vector<int>> maze;
    float cellSize;
    int width, height;
    int cellCount;
    bool faceCamera;

    std::unordered_map<char, glm::vec3> normalMap;

    std::vector<MeshPtr> cellsMesh;
    std::vector<glm::mat4> cellsTranslations;

    ShaderProgramPtr shader;

    CameraMoverPtr faceCameraPtr;
    CameraMoverPtr orbitCameraPtr;


    std::vector<char> getWalls(int i, int j);
    void fillCellVertices(int i, int j, std::vector<glm::vec3> & cellVertices);
    void addCell(int i, int j, const std::vector<char> & walls);

    static std::vector<glm::vec3> getWallVertices(char type, const std::vector<glm::vec3> & cellVertices);

    MeshPtr makeCellMesh(int i, int j, std::vector<char> walls);

    bool checkCell(int i, int j, float x, float y, int pos_i, int pos_j);
};


class MazeFreeCameraMover : public CameraMover
{
public:
    MazeFreeCameraMover( MazeApplication* mazeApp);

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;

protected:
    glm::vec3 _pos;
    glm::quat _rot;

    //Положение курсора мыши на предыдущем кадре
    double _oldXPos = 0.0;
    double _oldYPos = 0.0;

    MazeApplication* maze;
};


#endif //STUDENTTASKS2017_MAZE_H
