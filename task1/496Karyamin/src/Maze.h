//
// Created by avk on 15.03.18.
//

#ifndef STUDENTTASKS2017_MAZE_H
#define STUDENTTASKS2017_MAZE_H


#include <vector>
#include <memory>

struct Point {
    float x, y, z;
    Point(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {}
};

struct Wall {
    Point px, py;
    Wall(Point b, Point e) : px(b), py(e) {}

    typedef float f;
    Wall(f x1, f y1, f z1, f x2, f y2, f z2) : px(x1, y1, z1), py(x2, y2, z2) {}

};

bool between(float v, float a, float b);

class Maze {
public:
    Maze(std::size_t x, std::size_t y)
            : _xSize(x), _ySize(y) {}

    void addWall(Point beg, Point end);

    void addWall(Wall wall);

    void addWalls(const std::vector<Wall>& walls);

    const std::vector<Wall>& getWalls() const;

    std::size_t width();
    std::size_t height();
private:
    std::vector<Wall> _walls;
    std::size_t _xSize, _ySize;
};

typedef std::shared_ptr<Maze> MazePtr;

#endif //STUDENTTASKS2017_MAZE_H
