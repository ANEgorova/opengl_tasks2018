#include <iostream>

#include "SurfaceApplication.hpp"

SurfaceApplication::SurfaceApplication() {
    camera_handler_ = std::make_shared<FreeCameraHandler>();
}

void SurfaceApplication::makeScene() {
    mesh_ = calculateSurface(1, 80, 3);
    mesh_->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
    shader_ = std::make_shared<ShaderProgram>("493LatypovData/shader.vert",
                                              "493LatypovData/shader.frag");

    // Installs a program object as part of current rendering state
    shader_->use();
    shader_->setMat4Uniform("model_matrix", mesh_->GetModelMatrix());
}

void SurfaceApplication::draw() {
    int width, height;
    glfwGetFramebufferSize(window_, &width, &height);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Modifies the value of a uniform variable array
    shader_->setMat4Uniform("view_matrix", camera_.view_matrix);
    shader_->setMat4Uniform("projection_matrix", camera_.projection_matrix);

    mesh_->draw();
}