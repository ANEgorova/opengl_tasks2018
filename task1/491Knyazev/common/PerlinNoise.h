#pragma once

#include <cmath>

struct Vector2D {
    float x;
    float y;
    Vector2D(float x_, float y_) : x(x_), y(y_) {}
};

class PerlinNoise {
public:
    PerlinNoise(unsigned int num_of_octaves=5, float persistence=0.5f);
    float perlin_noise_2D(float x, float y);
private:
    float cosine_interpolation(float a, float b, float x);
    float smoothed_noise(float x, float y);
    float noise(int x, int y);
    float interpolated_noise(float x, float y);
    unsigned int num_of_octaves;
    float persistence;
};
