#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include "common/Common.h"

MeshPtr makeHyperboloid(const float radius, const unsigned int N = 1000) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    unsigned int M = N / 2;

    for (unsigned int i = 0; i < M; i++) {
        double z = (double)i / M * 2;
        double z1 = z + 2.0f / M;
        double r = sqrt(z * z);
        double r1 = sqrt(z1 * z1);

        for (unsigned int j = 0; j < N; j++) {
            double phi = 2.0f * glm::pi<float>() * j / N;
            double phi1 = phi + 2.0f * glm::pi<float>() / N;

            //Первый треугольник, образующий квадрат
            vertices.emplace_back(glm::vec3(cos(phi) * r, sin(phi) * r, r *r));
            vertices.emplace_back(glm::vec3(cos(phi1) * r, sin(phi1) * r, r *r));
            vertices.emplace_back(glm::vec3(cos(phi) * r1, sin(phi) * r1, r1 * r1));

            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r, 2. * sin(phi) * r, -1.)));
            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r, 2. * sin(phi1) * r, -1.)));
            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r1, 2. * sin(phi) * r1, -1.)));

            texcoords.emplace_back(glm::vec2((float)(j + 1) / N, (float)i / M));
            texcoords.emplace_back(glm::vec2((float)j / N, (float)i / M));
            texcoords.emplace_back(glm::vec2((float)(j + 1) / N, (float)(i + 1) / M));

            //Второй треугольник, образующий квадрат
            vertices.emplace_back(glm::vec3(cos(phi1) * r1, sin(phi1) * r1, r1 * r1));
            vertices.emplace_back(glm::vec3(cos(phi1) * r, sin(phi1) * r, r *r));
            vertices.emplace_back(glm::vec3(cos(phi) * r1, sin(phi) * r1, r1 * r1));

            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r1, 2. * sin(phi1) * r1, -1.)));
            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi1) * r, 2. * sin(phi1) * r, -1.)));
            normals.push_back(glm::normalize(glm::vec3(2. * cos(phi) * r1, 2. * sin(phi) * r1, -1.)));

            texcoords.emplace_back(glm::vec2((float)j / N, (float)(i + 1) / M));
            texcoords.emplace_back(glm::vec2((float)j / N, (float)i / M));
            texcoords.emplace_back(glm::vec2((float)(j + 1) / N, (float)(i + 1) / M));

        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

    return mesh;
}

// ДОЛЖНО БЫТЬ ТРИ ИСТОЧНИКА
constexpr unsigned int LightNum = 2;

/**
Пример с текстурированием разных 3д-моделей
*/
class SampleApplication : public Application
{
public:
    MeshPtr _figure;

    // Маркер источника света
    MeshPtr _lightMarker;

    // Идентификатор шейдерной программы
    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    // Координаты первого источника света - точечный источник, который движется вокруг поверхности
    float _lr0 = 5.0;
    float _phi0 = 0.0;
    float _theta0 = glm::pi<float>() * 0.15f;

    //Координаты второго источника света - точечный источник, который стоит на месте
    float _lr1 = 4.5;
    float _phi1 = 0.0;
    float _theta1 = glm::pi<float>() * 0.2f;

    // массив, хранящий переменные за все 3 источника света
    LightInfo _light[LightNum];

    TexturePtr _myTexture;

    GLuint _sampler;

    void makeScene() override
    {
        Application::makeScene();

        //Создание и загрузка мешей
        _figure = makeHyperboloid(0.3, 30);
        _figure->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        // Сфера - источник света
        _lightMarker = makeSphere(0.2f);

        //Инициализация шейдеров
        _shader = std::make_shared<ShaderProgram>("494EgorovaData/texture.vert", "494EgorovaData/texture.frag");
        _markerShader = std::make_shared<ShaderProgram>("494EgorovaData/marker.vert", "494EgorovaData/marker.frag");

        //Инициализация значений переменных освещения
        // Движущийся белый источник
        _light[0].position = glm::vec3(glm::cos(_phi0) * glm::cos(_theta0), glm::sin(_phi0) * glm::cos(_theta0), glm::sin(_theta0)) * _lr0;
        _light[0].ambient = glm::vec3(0.2, 0.2, 0.2); // Цвет окружающего света
        _light[0].diffuse = glm::vec3(0.8, 0.8, 0.8); // Диффузный цвет
        _light[0].specular = glm::vec3(1.0, 1.0, 1.0); // Бликовый цвет

        // Красный источник
        _light[1].position = glm::vec3(0.0, 3.0, 3.0);
        _light[1].ambient = glm::vec3(0.2, 0.0, 0.0);
        _light[1].diffuse = glm::vec3(0.8, 0.0, 0.0);
        _light[1].specular = glm::vec3(1.0, 1.0, 1.0);

        //Загрузка и создание текстур
        _myTexture = loadTexture("494EgorovaData/grass.jpg");

        // Настройка текстуры
        // Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        _phi0 += 0.002;

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);

        // Форма для изменения параметров точечного (нулевого источника) источника
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {

                ImGui::ColorEdit3("ambient", glm::value_ptr(_light[0].ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light[0].diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light[0].specular));

                ImGui::SliderFloat("radius", &_lr0, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi0, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta0, 0.0f, glm::pi<float>());

            }
        }
        ImGui::End();
    }

    void draw() override {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Подключаем шейдеры
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);


        _light[0].position = glm::vec3(glm::cos(_phi0) * glm::cos(_theta0), glm::sin(_phi0) * glm::cos(_theta0),
                                       glm::sin(_theta0)) * _lr0;
        _light[1].position = glm::vec3(glm::cos(_phi1) * glm::cos(_theta1), glm::sin(_phi1) * glm::cos(_theta1),
                                       glm::sin(_theta1)) * _lr1;

        for (unsigned int i = 0; i < LightNum; i++) {
            std::ostringstream str;
            str << "light[" << i << ']';

            glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light[i].position, 1.0));

            //копируем положение уже в системе виртуальной камеры
            _shader->setVec3Uniform(str.str() + ".pos", lightPosCamSpace);
            _shader->setVec3Uniform(str.str() + ".La", _light[i].ambient);
            _shader->setVec3Uniform(str.str() + ".Ld", _light[i].diffuse);
            _shader->setVec3Uniform(str.str() + ".Ls", _light[i].specular);
        }

        GLuint textureUnitForDiffuseTex = 0;

        if (USE_DSA) {
            glBindTextureUnit(textureUnitForDiffuseTex, _myTexture->texture());
            glBindSampler(textureUnitForDiffuseTex, _sampler);
        } else {
            glBindSampler(textureUnitForDiffuseTex, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0
            _myTexture->bind();
        }


        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _myTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);

        _shader->setMat4Uniform("modelMatrix", _figure->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(
                _camera.viewMatrix * _figure->modelMatrix()))));

        //Рисуем меш
        _figure->draw();

        // Рисуем источники света
        {
            _markerShader->use();

            for (unsigned int i = 0; i < LightNum; i++) {
                _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix
                                                           * glm::translate(glm::mat4(1.0f), _light[i].position));
                _markerShader->setVec4Uniform("color", glm::vec4(_light[i].diffuse, 1.0f));
                _lightMarker->draw();
            }
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);

    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
