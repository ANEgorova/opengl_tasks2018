/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};

uniform LightInfo light[4];

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;



void main()
{
    vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb; 
    vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции

    vec3 color = vec3(0.0);

    for (int i = 0; i < 4; i++) {

        vec3 lightDirCamSpace = normalize(light[i].pos - posCamSpace.xyz); //направление на источник света
        float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
        float distance = length(light[i].pos - posCamSpace.xyz);
        float attenuationCoef = 2.0 / (1.0 + 2.0 * distance);

        if (i != 3) {
            color += diffuseColor * (light[i].La + light[i].Ld * NdotL);
        }

        if (i == 3) {
            float NdotV = max(dot(normal, viewDirection), 0.0); //свет от камеры l = v
            float tangle = acos(dot(vec3(0, 0, 1), viewDirection));
            color += max(diffuseColor*(light[i].La + light[i].Ld * NdotV) * (((0.1 - tangle) / tangle - 1)), 0.0);
        }

        if (NdotL > 0.0) {
            vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света
            float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
            blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
            color += light[i].Ls * Ks * blinnTerm;
        }
	}

	fragColor = vec4(color, 1.0);
}